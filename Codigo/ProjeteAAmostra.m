%Projeta uma nova amostra no espa�o vetorial P
%Entrada:
%   amostra = amostra a ser projetada
%   mn = m�dia de cada coluna da matriz de dados
%   P = dados no novo espa�o vetorial (autofaces no caso de imagens)
%Sa�da:
%   amostra = amostra no novo espa�o vetorial
function amostra = ProjeteAAmostra(amostra, mediaDeCadaColunaMatrizDados, p)
    amostra = reshape(amostra, [size(amostra,1) * size(amostra,2), 1]);
    amostra = double(amostra) - mediaDeCadaColunaMatrizDados;
    amostra = p' * amostra;
end