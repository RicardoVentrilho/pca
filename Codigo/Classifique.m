%Classificador: Dist�ncia Euclidiana
%Entrada:
%   PC = Conjunto de treinamento
%   x = amostra a ser comparada
%Sa�da
%   d = posi��o do item com a menor dist�ncia euclidiana entre a amostra e as
%colunas do conjunto de treinamento
function posicaoDaMenorDistacia = Classifique(pc, amostra)
    menorDistancia = norm(pc(:,1) - amostra);
    posicaoDaMenorDistacia = 1;
    
    for j = 2:size(pc)
        distanciaDeEuclides = norm(pc(:, j) - amostra);
        
        if menorDistancia > distanciaDeEuclides
            posicaoDaMenorDistacia = j;
            menorDistancia = distanciaDeEuclides;
        end
    end
end