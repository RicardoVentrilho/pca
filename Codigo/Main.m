clear all, close all

QUANTIDADE_TREINO = Constantes.QUANTIDADE_TREINO;
QUANTIDADE_TESTE = Constantes.QUANTIDADE_TESTE;
TAMANHO_DA_BASE = Constantes.TAMANHO_DA_BASE;
       
[matrizDeDados, vetorDeIndice] = LeiaAsImagens(TAMANHO_DA_BASE, 1, QUANTIDADE_TREINO);

[P, PC, MN] = GereOsPCs(matrizDeDados);

while(1)
   enderecoDaImagem = input('Informe a imagem (entre apůstrofes): ');
   
   if (enderecoDaImagem == 0)
       break;
   end
   
   imagem = imread(strcat(Constantes.DIRETORIO_DAS_IMAGENS, strcat('\', enderecoDaImagem)));
   posicaoDaImagemClassificada = Classifique(PC, ProjeteAAmostra(imagem, MN, P));
   
   figure;
   
   fprintf('Pessoa obtida pela PCA: %d, pessoa informada: %s\n', vetorDeIndice(posicaoDaImagemClassificada), enderecoDaImagem);
   imshowpair(reshape(matrizDeDados(:, posicaoDaImagemClassificada), 112, 92), imagem, 'montage');
   
   clear im, clear x, clear d
end