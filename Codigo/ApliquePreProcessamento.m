function imagemProcessada = ApliquePreProcessamento(imagem)
    imagem = imcrop(imagem, [65, 60, 125, 170]);
    filtro = ones(3) / 24;
    imagem = imfilter(imagem, filtro);
    imagem = power(double(imagem), 1.1);
    imagem = histeq(uint8(imagem));
    imagem = imadjust(imagem, [0.3 0.9]);
    imagemProcessada = uint8(imagem);
end