function [matrizDeDadosTreino, vetorDeIndiceTreino, matrizDeDadosTeste, vetorDeIndiceTeste] = LeiaAsJaponesasComExpressoes(taxaDeTreino)
    japonesas = ObtenhaOsNomesDasJaponesas();
    expressoes = ObtenhaOsNomesDasExpressoes();
    
    matrizDeDadosTreino = [];
    matrizDeDadosTeste = [];
    vetorDeIndiceTreino = ["string"];
    vetorDeIndiceTeste = ["string"];
    
    for indiceJaponesa = 1:size(japonesas, 1)
        japonesa = japonesas(indiceJaponesa,:);
        
        for indiceExpressao = 1:size(expressoes, 1)
            expressao = expressoes(indiceExpressao,:);
            nome = strcat(japonesa, '.', expressao);
            
            [matrizTreino, matrizTeste] = LeiaAJaponesaComExpressao(japonesa, expressao, taxaDeTreino);
            
            %L� Treino
            tamanhoPorColunaTreino = size(matrizTreino, 2);
            matrizDeDadosTreino = [matrizDeDadosTreino matrizTreino];
            tamanhoTotalTreino = size(matrizDeDadosTreino, 2);
            vetorDeIndiceTreino((tamanhoTotalTreino - tamanhoPorColunaTreino + 1):tamanhoTotalTreino) = nome;
            
            %L� Teste
            tamanhoPorColunaTeste = size(matrizTeste, 2);
            matrizDeDadosTeste = [matrizDeDadosTeste matrizTeste];
            tamanhoTotalTeste = size(matrizDeDadosTeste, 2);
            vetorDeIndiceTeste((tamanhoTotalTeste - tamanhoPorColunaTeste + 1):tamanhoTotalTeste) = nome;
        end
    end
end