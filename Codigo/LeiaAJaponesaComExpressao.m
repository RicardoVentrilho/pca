function [matrizDeDadosTreino, matrizDeDadosTeste] = LeiaAJaponesaComExpressao(pessoa, expressao, taxaDeTreino)
    DIRETORIO_DAS_IMAGENS = Constantes.DIRETORIO_DAS_IMAGENS;
    
    diretorioAtual = pwd;
    
    expressoesNoDiretorio = dir(fullfile(DIRETORIO_DAS_IMAGENS, strcat(pessoa, '.', expressao, '*.tiff')));
    quantidadeDeExpressoes = length(expressoesNoDiretorio);
    
    %Pegando as quantidades de teste
    quantidadeDeExpressoesParaTeste = ceil(quantidadeDeExpressoes * ((100 - taxaDeTreino) / 100));
    
    matrizDeDadosTreino = [];
    matrizDeDadosTeste = [];
    
    %Salvando a matriz de teste
    for indiceExpressao = 1:quantidadeDeExpressoesParaTeste
        expressaoDaPessoa = imread(fullfile(DIRETORIO_DAS_IMAGENS, expressoesNoDiretorio(indiceExpressao).name));
        expressaoDaPessoa = ApliquePreProcessamento(expressaoDaPessoa);
        
        expressaoDaPessoaEmUmaColuna = reshape(expressaoDaPessoa, [size(expressaoDaPessoa,1) * size(expressaoDaPessoa,2), 1]);

        matrizDeDadosTeste = [matrizDeDadosTeste expressaoDaPessoaEmUmaColuna];
    end
    
    %Salvando a matriz de treino
    for indiceExpressao = (quantidadeDeExpressoesParaTeste + 1):quantidadeDeExpressoes
        expressaoDaPessoa = imread(fullfile(DIRETORIO_DAS_IMAGENS, expressoesNoDiretorio(indiceExpressao).name));
        expressaoDaPessoa = ApliquePreProcessamento(expressaoDaPessoa);
        
        expressaoDaPessoaEmUmaColuna = reshape(expressaoDaPessoa, [size(expressaoDaPessoa,1) * size(expressaoDaPessoa,2), 1]);

        matrizDeDadosTreino = [matrizDeDadosTreino expressaoDaPessoaEmUmaColuna];
    end
    
    cd(diretorioAtual);
end