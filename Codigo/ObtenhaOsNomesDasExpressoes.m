function [expressoes] = ObtenhaOsNomesDasExpressoes()
    DIRETORIO_DAS_IMAGENS = Constantes.DIRETORIO_DAS_IMAGENS;
    
    diretorioAtual = pwd;
    
    imagensNoDiretorio = dir(fullfile(DIRETORIO_DAS_IMAGENS, '*.tiff'));
    quantidadeDeImagens = length(imagensNoDiretorio);
    expressoes = [];
    
    for indice = 1:quantidadeDeImagens
        nomeDaJaponesa = imagensNoDiretorio(indice).name;
        expressoes = cat(1, expressoes, nomeDaJaponesa(4:5));
    end
    
    expressoes = unique(expressoes, 'rows');
    
    cd(diretorioAtual);
end