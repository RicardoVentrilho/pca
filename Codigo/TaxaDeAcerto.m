function [total, acertos, taxa] = TaxaDeAcerto(p, pc, mn)
    QUANTIDADE_TREINO = Constantes.QUANTIDADE_TREINO;
    QUANTIDADE_TESTE = Constantes.QUANTIDADE_TESTE;
    TAMANHO_DA_BASE = Constantes.TAMANHO_DA_BASE;
    
    [~, vetorDeIndice ] = LeiaAsImagens(TAMANHO_DA_BASE, 1, QUANTIDADE_TREINO);
    [matrizDeDadosTeste, vetorDeIndiceTeste] = LeiaAsImagens(TAMANHO_DA_BASE, QUANTIDADE_TREINO + 1, QUANTIDADE_TREINO + QUANTIDADE_TESTE);
    
    [~, quantidadeDeImagensTeste] = size(vetorDeIndiceTeste);
    acertos = 0;
    
    for i = 1:quantidadeDeImagensTeste
        imagemTeste = reshape(matrizDeDadosTeste(:, i), 112, 92);
        posicaoDaImagemClassificada = Classifique(pc, ProjeteAAmostra(imagemTeste, mn, p));
        
        if vetorDeIndice(posicaoDaImagemClassificada) == vetorDeIndiceTeste(i)
            acertos = acertos + 1;
        end
    end
    
    total = quantidadeDeImagensTeste;
    taxa = (acertos / total) * 100;
end