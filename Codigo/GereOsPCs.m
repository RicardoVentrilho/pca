%Gera a matriz de componentes principais
%Entrada:
%   data = matriz de dados. Cada amostra de medi��es � representada em uma
%coluna
%sa�da:
%   P = dados no novo espa�o vetorial (autofaces no caso de imagens)
%   PC = matriz componentes principais
%   mn = m�dia de cada coluna da matriz de dados
function [P PC MN] = GereOsPCs(matrizDeDados)

    [~, N] = size(matrizDeDados);
    
    MN = mean(matrizDeDados, 2); %calcular a m�dia
    
    matrizDeDados = double(matrizDeDados) - repmat(MN, 1, N); %imagens - media

    covariancia = matrizDeDados' * matrizDeDados; %matriz de covari�ncia
    
    [PC, V] = eig(covariancia); %autovetores e autovalores

    V = diag(V); 

    clear covariancia, clear M, clear N;

    %ordenar autovetores e autovalores

    [~, rindices] = sort(-1 * V);
    V = V(rindices); %ordenou!!
    V = diag(V);
    PC = PC(:,rindices);

    clear rindices, clear V;

    %c�lculo das autofaces

    P = matrizDeDados * PC;
    i = size(PC,2);

    clear PC;

    %proje��o das imagens no espa�o de faces
    
    PC = [];
    for j = 1:i
        temp = P' * matrizDeDados(:,j);
        PC = [PC temp];
    end

    clear i, clear j, clear temp;
end