%Projeta uma nova amostra no espa�o vetorial P
%Entrada:
%   amostra = amostra a ser projetada
%   mn = m�dia de cada coluna da matriz de dados
%   P = dados no novo espa�o vetorial (autofaces no caso de imagens)
%Sa�da:
%   amostra = amostra no novo espa�o vetorial
function amostra = ProjeteAAmostraComCaracteristica(amostra, mediaDeCadaColunaMatrizDados, p)
    amostra = double(amostra) - mediaDeCadaColunaMatrizDados;
    amostra = p' * amostra;
end