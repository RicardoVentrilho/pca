function [japonesas] = ObtenhaOsNomesDasJaponesas()
    DIRETORIO_DAS_IMAGENS = Constantes.DIRETORIO_DAS_IMAGENS;
    
    diretorioAtual = pwd;
    
    imagensNoDiretorio = dir(fullfile(DIRETORIO_DAS_IMAGENS, '*.tiff'));
    quantidadeDeImagens = length(imagensNoDiretorio);
    japonesas = [];
    
    for indice = 1:quantidadeDeImagens
        nomeDaJaponesa = imagensNoDiretorio(indice).name;
        japonesas = cat(1, japonesas, nomeDaJaponesa(1:2));
    end
    
    japonesas = unique(japonesas, 'rows');
    
    cd(diretorioAtual);
end