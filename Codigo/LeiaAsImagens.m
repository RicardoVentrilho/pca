function [matrizDeDados, vetorDeIndice] = LeiaAsImagens(tamanhoDaBase, inicio, fim)
    DIRETORIO_DAS_IMAGENS = Constantes.DIRETORIO_DAS_IMAGENS;
    NOME_DA_PASTA_DAS_IMAGENS = Constantes.NOME_DA_PASTA_DAS_IMAGENS;
    
    matrizDeDados = [];
    vetorDeIndice = [];
    diretorioAtual = pwd;
    
    for indice = 1:tamanhoDaBase
        diretorioBaseDaPasta = strcat(DIRETORIO_DAS_IMAGENS, NOME_DA_PASTA_DAS_IMAGENS);
        diretorioDaPasta = strcat(diretorioBaseDaPasta, int2str(indice));
        cd(diretorioDaPasta);
        
        for i = inicio:fim
            imagem = imread(strcat(int2str(i), '.pgm'));
            imagem = power(double(imagem), 0.2); %Corre��o Gama
            y = reshape(imagem, [size(imagem,1) * size(imagem,2), 1]);
            matrizDeDados = [matrizDeDados , y];
            posicao = ((indice - 1) * (fim - inicio + 1)) + (i - fim + (fim - inicio + 1));
            vetorDeIndice(posicao) = indice;
        end
    end
    
    cd(diretorioAtual);
end