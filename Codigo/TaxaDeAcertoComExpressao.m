function [total, acertos, taxa] = TaxaDeAcertoComExpressao()
    [matrizDeDados, vetorDeIndice, matrizDeDadosTeste, vetorDeIndiceTeste] = LeiaAsJaponesasComExpressoes(70);
    [P, PC, MN] = GereOsPCs(matrizDeDados);
    
    [quantidadeDeImagensTeste] = size(vetorDeIndiceTeste, 2);
    acertos = 0;
    
    for indice = 1:quantidadeDeImagensTeste
        imagemColuna = matrizDeDadosTeste(:, indice);
        
        posicaoDaImagemClassificada = Classifique(PC, ProjeteAAmostraComCaracteristica(imagemColuna, MN, P));
        
        if vetorDeIndice(posicaoDaImagemClassificada) == vetorDeIndiceTeste(indice)
            acertos = acertos + 1;
        end
    end
    
    total = quantidadeDeImagensTeste;
    taxa = (acertos / total) * 100;
end