function [total, acertos, taxa] = TaxaDeAcertoSemExpressao()
    [matrizDeDados, vetorDeIndice, matrizDeDadosTeste, vetorDeIndiceTeste] = LeiaAsJaponesasComExpressoes(70);
    [P, PC, MN] = GereOsPCs(matrizDeDados);
    
    [quantidadeDeImagensTeste] = size(vetorDeIndiceTeste, 2);
    acertos = 0;
    
    for indice = 1:quantidadeDeImagensTeste
        imagemColuna = matrizDeDadosTeste(:, indice);
        
        posicaoDaImagemClassificada = Classifique(PC, ProjeteAAmostraComCaracteristica(imagemColuna, MN, P));
        
        nomeGerado = extractBetween(vetorDeIndice(posicaoDaImagemClassificada), 1, 2);
        nomeReal = extractBetween(vetorDeIndiceTeste(indice), 1, 2);
        
        if nomeGerado == nomeReal
            acertos = acertos + 1;
        end
    end
    
    total = quantidadeDeImagensTeste;
    taxa = (acertos / total) * 100;
end